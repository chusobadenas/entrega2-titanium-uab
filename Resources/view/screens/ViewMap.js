function ViewMap() {

  this.view = null;

  this.loadAllMarkers = function() {
    var Marker = require('view/components/Marker');
    var defaultMarkers = require('data/DefaultMarkers').get();
    var allMarkers = [];

    // Add defaults
    for (var i = 0; i < defaultMarkers.length; i++) {
      allMarkers.push(defaultMarkers[i]);
    }

    // Read stored markers
    var sMarkers = Ti.App.Properties.getList('markers');

    // Add stored markers
    if (sMarkers) {
      for (var i = 0; i < sMarkers.length; i++) {
        var marker = new Marker(sMarkers[i].title, sMarkers[i].subtitle, sMarkers[i].link, sMarkers[i].latitude, sMarkers[i].longitude, sMarkers[i].imageUrl);
        allMarkers.push(marker.view);
      }
    }

    return allMarkers;
  };

  this.build = function() {
    // DATA
    var Map = require('ti.map');
    var Marker = require('view/components/Marker');
    var self = this;

    // Load markers
    var allMarkers = this.loadAllMarkers();

    // Show the map
    var mapView = Map.createView({
      mapType : Map.NORMAL_TYPE,
      region : {
        latitude : 41.392006,
        longitude : 2.174209,
        latitudeDelta : 0.1,
        longitudeDelta : 0.1
      },
      animate : true,
      regionFit : true,
      userLocation : true,
      annotations : allMarkers
    });

    // Click on image annotation
    if (utils.isAndroid()) {
      mapView.addEventListener('click', function(e) {
        if (e.clicksource === 'leftPane') {
          Ti.Platform.openURL(e.annotation.link);
        }
      });
    }

    // Add marker event listener
    Ti.App.addEventListener('add_marker', function(param) {
      if (self.view != null) {
        // Create marker
        var marker = new Marker(param.title, param.subtitle, param.link, param.latitude, param.longitude, param.imageUrl);
        self.view.addAnnotation(marker.view);

        /*** SAVE PROPERTIES ***/
        var sMarkers = Ti.App.Properties.getList('markers');

        if (sMarkers) {
          sMarkers.push(param);
        } else {
          sMarkers = [param];
        }

        Ti.App.Properties.setList('markers', sMarkers);
        /*** END SAVE ***/

        // Show dialog
        var dialog = utils.createDialog('', L('marker_added_success'));
        dialog.show();

        // Switch tab and center map
        dialog.addEventListener('click', function(e) {
          self.view.setRegion({
            latitude : param.latitude,
            longitude : param.longitude,
            latitudeDelta : 0.1,
            longitudeDelta : 0.1
          });
          tabGroup.setActiveTab(tab1);
        });
      }
    });

    // Remove all markers event listener
    Ti.App.addEventListener('remove_all_markers', function() {
      if (self.view != null) {
        // Erase stored data
        Ti.App.Properties.setList('markers', null);

        // Remove from map view
        self.view.removeAllAnnotations();

        // Load defaults again
        var defaultMarkers = self.loadAllMarkers();
        self.view.addAnnotations(defaultMarkers);

        // Show dialog
        var dialog = utils.createDialog('', L('markers_deleted_success'));
        dialog.show();
      }
    });

    // Save view
    this.view = mapView;
  };

  this.build();
}

module.exports = ViewMap;
