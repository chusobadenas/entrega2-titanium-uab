function AddMarker() {

  this.view = null;

  this.createButton = function(title) {
    return Ti.UI.createButton({
      title : title,
      height : utils.isAndroid() ? 42 : 32,
      width : Ti.UI.SIZE,
      top : 5,
      bottom : 5
    });
  };

  this.createLabel = function(text) {
    return Ti.UI.createLabel({
      color : '#000',
      font : {
        fontSize : defaultFontSize,
        fontWeight : 'bold'
      },
      height : Ti.UI.SIZE,
      width : 70,
      text : text,
      left : 0,
      top : 5,
      bottom : 5
    });
  };

  this.createTextField = function(type) {
    return Ti.UI.createTextField({
      autocapitalization : false,
      autocorrect : false,
      borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
      clearButtonMode : Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
      color : '#000',
      focusable : true,
      font : {
        fontSize : defaultFontSize,
        fontWeight : 'normal'
      },
      keyboardType : type,
      returnKeyType : Ti.UI.RETURNKEY_DONE,
      height : Ti.UI.SIZE,
      width : Ti.UI.FILL,
      left : 15,
      top : 5,
      right : 0,
      bottom : 5
    });
  };

  this.createView = function(orientation) {
    var aView = Ti.UI.createView({
      height : Ti.UI.SIZE
    });

    if (orientation) {
      aView.layout = orientation;
    }

    return aView;
  };

  this.createRow = function() {
    return Ti.UI.createTableViewRow({
      heigth : 'auto',
      backgroundColor : 'transparent'
    });
  };

  this.insertRow = function(tableData, view) {
    var row = this.createRow();
    row.add(view);

    tableData.push(row);
  };

  this.checkValues = function(array) {
    var result = true;
    var errorMsg = null;
    var urlRegex = /^(http[s]?:\/\/)(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    var numberRegex = /\d+(\.\d{1,9})?/;

    var title = array[0].value;
    var subtitle = array[1].value;
    var link = array[2].value;
    var latitude = array[3].value;
    var longitude = array[4].value;

    if (!title || !subtitle || !link || !latitude || !longitude) {
      result = false;
      errorMsg = L('error_emtpy_fields');
    } else if (!urlRegex.test(link)) {
      result = false;
      errorMsg = L('error_link_field');
    } else if (!numberRegex.test(latitude) || !numberRegex.test(longitude)) {
      result = false;
      errorMsg = L('error_location_fields');
    }

    if (errorMsg) {
      var dialog = utils.createDialog(L('validation_error'), errorMsg);
      dialog.show();
    }

    return result;
  };

  this.build = function() {
    var self = this;
    var fields = [L('title'), L('subtitle'), L('link'), L('latitude'), L('longitude')];
    var types = [Ti.UI.KEYBOARD_TYPE_DEFAULT, Ti.UI.KEYBOARD_TYPE_DEFAULT, Ti.UI.KEYBOARD_TYPE_URL, Ti.UI.KEYBOARD_DECIMAL_PAD, Ti.UI.KEYBOARD_DECIMAL_PAD];
    var tableData = [];
    var fieldValues = [];

    // Create form
    for (var i = 0; i < fields.length; i++) {
      var field = fields[i];
      var type = types[i];

      var hView = this.createView('horizontal');
      var label = this.createLabel(field);
      var txtField = this.createTextField(type);
      fieldValues.push(txtField);

      hView.add(label);
      hView.add(txtField);

      this.insertRow(tableData, hView);
    }

    // Delete and Add buttons
    var hView = this.createView();
    hView.width = Ti.UI.FILL;

    var dButton = this.createButton(L('delete'));
    dButton.left = 0;
    dButton.color = '#F00';

    var aButton = this.createButton(L('add'));
    aButton.right = 0;

    // ADD ACTION
    aButton.addEventListener('click', function(e) {
      // VALIDATION
      if (self.checkValues(fieldValues)) {
        // Store data
        var param = {
          'title' : fieldValues[0].value,
          'subtitle' : fieldValues[1].value,
          'link' : fieldValues[2].value,
          'latitude' : parseFloat(fieldValues[3].value),
          'longitude' : parseFloat(fieldValues[4].value),
          'imageUrl' : 'http://icons.iconarchive.com/icons/alecive/flatwoken/512/Apps-Google-Maps-icon.png'
        };

        Ti.App.fireEvent('add_marker', param);

        // Clear form
        for (var i = 0; i < fieldValues.length; i++) {
          fieldValues[i].setValue('');
        }

        // Focus
        fieldValues[0].focus();
      }
    });

    // DELETE ACTION
    dButton.addEventListener('click', function(e) {
      var dialog = utils.createConfirmDialog('', L('delete_message'));
      dialog.show();

      // Delete all markers
      dialog.addEventListener('click', function(e) {
        if (e.index === 1) {
          Ti.App.fireEvent('remove_all_markers');
        }
      });
    });

    hView.add(dButton);
    hView.add(aButton);
    this.insertRow(tableData, hView);

    // TABLE object
    var tableView = Ti.UI.createTableView({
      backgroundColor : '#FFF',
      data : tableData,
      separatorColor : 'transparent',
      separatorStyle : 'none',
      selectionStyle : 'none',
      scrollable : false,
      top : 20,
      right : 15,
      bottom : 10,
      left : 15,
      height : utils.isAndroid() ? 480 : Ti.UI.SIZE
    });

    // Save view
    this.view = tableView;
  };

  this.build();
}

module.exports = AddMarker;
