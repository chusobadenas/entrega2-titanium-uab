function AnnotationView(imageUrl, imageLink) {

  this.view = null;

  this.build = function() {
    var mainView = Ti.UI.createView({
      height : Ti.UI.SIZE,
      width : Ti.UI.SIZE
    });

    var imageView = Ti.UI.createImageView({
      image : imageUrl,
      height : 32,
      width : 32
    });
    mainView.add(imageView);

    // Open browser when user clicks the image
    imageView.addEventListener('click', function(e) {
      Ti.Platform.openURL(imageLink);
    });

    this.view = utils.isAndroid() ? mainView : imageView;
  };

  this.build();
}

module.exports = AnnotationView;
