function Marker(title, subtitle, link, latitude, longitude, imageUrl) {

  this.view = null;

  this.build = function() {
    var Map = require('ti.map');
    var AnnotationView = require('view/components/AnnotationView');

    // Create annotation view
    var aView = new AnnotationView(imageUrl, link);

    // Create marker
    var point = Map.createAnnotation({
      latitude : latitude,
      longitude : longitude,
      leftView : aView.view,
      link : link,
      title : title,
      subtitle : subtitle,
      pincolor : Map.ANNOTATION_RED
    });

    // Save view
    this.view = point;
  };

  this.build();
}

module.exports = Marker;
