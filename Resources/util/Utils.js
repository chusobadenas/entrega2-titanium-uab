function Utils() {

  this.isAndroid = function() {
    return (Ti.Platform.name == 'android');
  };

  this.isMobileWeb = function() {
    return (Ti.Platform.name == 'mobileweb');
  };

  this.isIOS = function() {
    return (Ti.Platform.name == 'iPhone OS');
  };

  this.isTizen = function() {
    return (Ti.Platform.name == 'TIZEN');
  };

  this.createDialog = function(title, message) {
    return Ti.UI.createAlertDialog({
      message : message,
      ok : 'OK',
      title : title
    });
  };

  this.createConfirmDialog = function(title, message) {
    return Ti.UI.createAlertDialog({
      cancel : 0,
      buttonNames : [L('cancel'), L('confirm')],
      message : message,
      title : title
    });
  };
}

module.exports = Utils;
