// Global variables
var Utils = require('/util/Utils');
var utils = new Utils();

var defaultFontSize = utils.isAndroid() ? 16 : 14;

// Create tab group
var tabGroup = Titanium.UI.createTabGroup();

/*** FIRST TAB ***/
var win1 = Titanium.UI.createWindow({
  title : L('title_tab1'),
  backgroundColor : '#FFF'
});

var ViewMap = require('view/screens/ViewMap');
var viewMap = new ViewMap();
win1.add(viewMap.view);

var tab1 = Titanium.UI.createTab({
  icon : '/images/view_map.png',
  title : L('title_tab1'),
  window : win1
});

/*** SECOND TAB ***/
var win2 = Titanium.UI.createWindow({
  title : L('title_tab2'),
  backgroundColor : '#FFF'
});

var AddMarker = require('view/screens/AddMarker');
var addMarker = new AddMarker();
win2.add(addMarker.view);

var tab2 = Titanium.UI.createTab({
  icon : '/images/add_mark.png',
  title : L('title_tab2'),
  window : win2
});

//  Add tabs
tabGroup.addTab(tab1);
tabGroup.addTab(tab2);

// open tab group
tabGroup.open();
