function getData() {
  var Marker = require('view/components/Marker');

  // La Sagrada Familia
  var point1 = new Marker('La Sagrada Familia', 
                          'Carrer de la Marina 266-270', 
                          'http://www.sagradafamilia.cat', 
                          41.404095, 
                          2.174582, 
                          'http://weinfo.com/wp-content/uploads/2015/11/meerdaagsvoortgezet2.jpg');

  // Zoo de Barcelona
  var point2 = new Marker('Zoo de Barcelona', 
                          'Parc de la Ciutadella', 
                          'http://www.zoobarcelona.cat', 
                          41.3883324, 
                          2.1862068, 
                          'http://www.viajarporespana.net/sites/viajarporespana.net/files/zoocorp.jpg');
  
  return [point1.view, point2.view];
}

exports.get = getData;
